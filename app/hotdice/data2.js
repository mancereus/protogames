var cards = {
    stack: [
        { color: 'lightblue', items: [1, 1, 1, 2, 3,0], val: "z" },
        { color: 'lightblue', items: [1, 1, 1, 2, 3,0], val: "z" },
        { color: 'lightblue', items: [1, 1, 1, 2, 3,0], val: "z" },
        { color: 'lightgreen', items: [1, 2, 2, 2, 3, 4], val: "z" },
        { color: 'lightgreen', items: [1, 2, 2, 2, 3, 4], val: "z" },
        { color: 'lightgreen', items: [1, 2, 2, 2, 3, 4], val: "z" },
        { color: 'lightyellow', items: [2,3,3, 3, 4,  5, 'X'], val: "z" },
        { color: 'lightyellow', items: [2,3,3, 3, 4,  5, 'X'], val: "z" },
        { color: 'lightyellow', items: [2,3,3, 3, 4,  5, 'X'], val: "z" },
        { color: 'orange', items: [3, 4, 4, 5, 5, 'X'], val: "z" },
        { color: 'orange', items: [3, 4, 4, 5, 5, 'X'], val: "z" },
        { color: 'orange', items: [3, 4, 4, 5, 5, 'X'], val: "z" },
    ],
    roll: [
        { color: 'lightblue', items: [1, 1, 2, 2, 3, 4], val: "z" },
        { color: 'lightgreen', items: [2, 3, 4, 5], val: "z" },
        { color: 'lightyellow', items: [3, 3, 4, 4, 5, 'X'], val: "z" },
        { color: 'orange', items: [3, 4, 4, 5, 5, 'X'], val: "z" },
    ],
}
