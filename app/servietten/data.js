var cards = [
    {name: "B", color: "orange"},
    {name: "C", color: "yellow"},
    {name: "A", color: "green"},
    {name: "D", color: "red"},
    {name: "E", color: "blue"},

    {name: "D", color: "red"},
    {name: "E", color: "blue"},
    {name: "B", color: "orange"},
    {name: "A", color: "green"},
    {name: "C", color: "yellow"},

    {name: "C", color: "yellow"},
    {name: "A", color: "green"},
    {name: "D", color: "red"},
    {name: "B", color: "orange"},
    {name: "E", color: "blue"},

    {name: "B", color: "orange"},
    {name: "E", color: "blue"},
    {name: "C", color: "yellow"},
    {name: "A", color: "green"},
    {name: "D", color: "red"},

    {name: "D", color: "red"},
    {name: "C", color: "yellow"},
    {name: "A", color: "green"},
    {name: "B", color: "orange"},
    {name: "E", color: "blue"},
]